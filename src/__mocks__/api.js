import loanInstallmentTestData from "../../tests/unit/data/loanInstallment";

export default {
  // eslint-disable-next-line no-undef
  getLoanData: jest.fn().mockResolvedValue(loanInstallmentTestData)
};
