import Vue from "vue";
import VueRouter from "vue-router";
import LoanCalculatorWidget from "@/views/LoanCalculatorWidget";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    component: LoanCalculatorWidget
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
