import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import VueCustomElement from "vue-custom-element";

Vue.config.productionTip = false;
Vue.use(VueCustomElement);

App.store = store;
App.router = router;
Vue.customElement("loan-calculator-widget", App);

/*
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
*/
