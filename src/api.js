import axios from "axios";
import httpAdapter from "axios/lib/adapters/http";

const instance = axios.create({
  baseURL: "http://www.mocky.io",
  adapter: httpAdapter
});

export default {
  getLoanData(loanData) {
    return instance
      .post("/v2/5e82319a2f000035002fb965", loanData)
      .then(result => result.data);
  }
};
