import loanInstallmentTestData from "../../../tests/unit/data/loanInstallment";

export default {
  // eslint-disable-next-line no-undef
  GET_LOANINSTALLMENT: jest.fn().mockResolvedValue(loanInstallmentTestData)
};
