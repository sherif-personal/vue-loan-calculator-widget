export default {
  SET_LOANDATA(state, loanData) {
    state.calculatedInstallmentData = { ...loanData };
  }
};
