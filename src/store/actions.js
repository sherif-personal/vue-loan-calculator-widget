import api from "@/api";

export default {
  GET_LOANINSTALLMENT({ commit }, { loanData }) {
    // eslint-disable-next-line no-async-promise-executor
    return new Promise(async (resolve, reject) => {
      try {
        const loanInstallmentInfo = await api.getLoanData(loanData);
        commit("SET_LOANDATA", loanInstallmentInfo);
        resolve(loanInstallmentInfo);
      } catch (error) {
        reject(error);
      }
    });
  }
};
