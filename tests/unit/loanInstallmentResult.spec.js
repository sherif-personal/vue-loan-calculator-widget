import { shallowMount } from "@vue/test-utils";
import LoanInstallmentResult from "@/components/LoanInstallmentResult";
import loanInstallmentTestData from "./data/loanInstallment";

describe("LoanInstallmentResult", () => {
  let props;

  const buildTestStructure = () => {
    const wrapper = shallowMount(LoanInstallmentResult, {
      propsData: props
    });

    return {
      wrapper,
      loanInstallmentAmount: () => wrapper.find(".loan-installment-amount")
    };
  };

  beforeEach(() => {
    props = {
      calculatedInstallmentData: loanInstallmentTestData
    };
  });

  it("renders component", () => {
    const { wrapper } = buildTestStructure();
    expect(wrapper.html()).toMatchSnapshot();
  });

  it("renders result in the component", () => {
    const { loanInstallmentAmount } = buildTestStructure();
    expect(loanInstallmentAmount().exists()).toBe(true);
    expect(loanInstallmentAmount().text()).toBe(
      props.calculatedInstallmentData.monthlyInstallment
    );
  });
});
