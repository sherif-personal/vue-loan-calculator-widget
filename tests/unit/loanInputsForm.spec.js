import { shallowMount } from "@vue/test-utils";
import LoanInputsForm from "@/components/LoanInputsForm";

describe("LoanInputsForm", () => {
  const buildTestStructure = () => {
    const wrapper = shallowMount(LoanInputsForm);

    return {
      wrapper,
      inputLoanAmount: () => wrapper.find(".input-loan-amount"),
      selectLoanDuration: () => wrapper.find(".select-loan-duration"),
      submitButton: () => wrapper.find(".submit")
    };
  };

  it("renders component", () => {
    const { wrapper } = buildTestStructure();
    expect(wrapper.html()).toMatchSnapshot();
  });

  it("renders form elements", () => {
    const {
      inputLoanAmount,
      selectLoanDuration,
      submitButton
    } = buildTestStructure();

    expect(inputLoanAmount().exists()).toBe(true);
    expect(selectLoanDuration().exists()).toBe(true);
    expect(submitButton().exists()).toBe(true);
  });

  it("calls 'submit' method on form submit", () => {
    const expectedInputData = {
      amount: "10000.00",
      duration: "1"
    };

    const {
      wrapper,
      inputLoanAmount,
      selectLoanDuration,
      submitButton
    } = buildTestStructure();

    inputLoanAmount().element.value = expectedInputData.amount;
    selectLoanDuration().element.value = expectedInputData.duration;

    submitButton().trigger("click");
    submitButton().trigger("submit");

    expect(wrapper.emitted().submitted[0]).toEqual([expectedInputData]);
  });
});
