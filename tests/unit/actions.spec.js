jest.mock("@/api");
import flushPromises from "flush-promises";
import actions from "@/store/actions";
import api from "@/api";
import loanInstallmentTestData from "./data/loanInstallment";

describe("store actions", () => {
  let commit;

  beforeEach(() => {
    commit = jest.fn();
  });

  it("Get loan monthly installment amount", async () => {
    const expectedInputData = {
      amount: "10000.00",
      duration: "1"
    };

    await actions.GET_LOANINSTALLMENT(
      { commit },
      { loanData: expectedInputData }
    );
    await flushPromises();

    expect(api.getLoanData).toHaveBeenCalledWith(expectedInputData);
    expect(commit).toHaveBeenCalledWith(
      "SET_LOANDATA",
      loanInstallmentTestData
    );
  });
});
