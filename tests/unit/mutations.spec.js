import mutations from "@/store/mutations";
import initialState from "@/store/state";
import loanInstallmentTestData from "./data/loanInstallment";

describe("mutations", () => {
  let state;

  beforeEach(() => {
    state = { ...initialState }
  });

  it("get loan installment data", () => {
    const expectedLoanData = loanInstallmentTestData;

    mutations.SET_LOANDATA(state, expectedLoanData);

    expect(state.calculatedInstallmentData).toEqual(expectedLoanData);
  });
});
