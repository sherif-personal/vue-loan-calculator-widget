jest.mock("@/store/actions");
import { shallowMount, createLocalVue } from "@vue/test-utils";
import Vuex from "vuex";
import LoanCalculatorWidget from "@/views/LoanCalculatorWidget";
import LoanInputsForm from "@/components/LoanInputsForm";
import LoanInstallmentResult from "@/components/LoanInstallmentResult";
import initialState from "@/store/state";
import actions from "@/store/actions";
import loanInstallmentTestData from "./data/loanInstallment";

const localVue = createLocalVue();
localVue.use(Vuex);

describe("LoanCalculatorWidget", () => {
  let state;

  const buildTestStructure = () => {
    const wrapper = shallowMount(LoanCalculatorWidget, {
      localVue,
      store: new Vuex.Store({
        state,
        actions
      })
    });

    return {
      wrapper,
      loanInputsForm: () => wrapper.find(LoanInputsForm),
      loanInstallmentResult: () => wrapper.find(LoanInstallmentResult)
    };
  };

  beforeEach(() => {
    jest.resetAllMocks();
    state = { ...initialState };
  });

  it("renders the component", () => {
    const { wrapper } = buildTestStructure();
    expect(wrapper.html()).toMatchSnapshot();
  });

  it("renders the main child components", () => {
    const { loanInputsForm, loanInstallmentResult } = buildTestStructure();

    expect(loanInputsForm().exists()).toBe(true);
    expect(loanInstallmentResult().exists()).toBe(true);
  });

  it("passes the calculated loan installment data result to the loanInstallmentResult component", () => {
    state.calculatedInstallmentData = loanInstallmentTestData;

    const { loanInstallmentResult } = buildTestStructure();

    expect(loanInstallmentResult().vm.calculatedInstallmentData).toBe(
      state.calculatedInstallmentData
    );
  });

  it("send loan amount and duration request once the submit click event triggered from LoanInputsForm component", () => {
    const receviedLoanAmountDuration = { amount: "10000.00", duration: "5" };
    const { loanInputsForm } = buildTestStructure();

    loanInputsForm().vm.$emit("submitted", receviedLoanAmountDuration);

    expect(actions.GET_LOANINSTALLMENT).toHaveBeenCalled();
    expect(actions.GET_LOANINSTALLMENT.mock.calls[0][1]).toEqual({
      loanAmountDuration: receviedLoanAmountDuration
    });
  });
});
