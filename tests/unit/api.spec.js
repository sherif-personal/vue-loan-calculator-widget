import flushPromises from "flush-promises";
import nock from "nock";
import api from "@/api";
import loanInstallmentTestData from "./data/loanInstallment";

describe("api service for loan installment", () => {
  it("Gets Loan monthly installment information", async () => {
    const expectedLoanDataInput = {
      amount: "10000.00",
      duration: "5"
    };

    const request = nock("http://www.mocky.io")
      .post("/v2/5e82319a2f000035002fb965", expectedLoanDataInput)
      .reply(200, loanInstallmentTestData);

    const result = await api.getLoanData(expectedLoanDataInput);

    await flushPromises();

    expect(result).toEqual(loanInstallmentTestData);
    expect(request.isDone()).toBe(true);
  });
});
