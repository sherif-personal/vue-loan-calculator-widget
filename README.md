# vue-loan-calculator-widget
Vue Js widget loan calculator built using vue cli 3. The widget can be embedded into your website by:
 - including the widget app js and css from dist folder
 - in your html insert <loan-calculator-widget></loan-calculator-widget> to render the widget

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
